from enum import Enum


class LoadBehavior(Enum):
    TRUNCATE_AND_LOAD = 1
    APPEND = 2
