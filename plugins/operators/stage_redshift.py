from airflow.contrib.hooks.aws_hook import AwsHook
from airflow.hooks.postgres_hook import PostgresHook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults

from helpers.load_behavior import LoadBehavior


class StageToRedshiftOperator(BaseOperator):
    copy_sql = """
            COPY {}
            FROM '{}'
            ACCESS_KEY_ID '{}'
            SECRET_ACCESS_KEY '{}'
            IGNOREHEADER {}
            REGION '{}'
            FORMAT AS JSON '{}'
        """
    ui_color = '#358140'

    @apply_defaults
    def __init__(self,
                 s3_bucket_name: str,
                 s3_key_name: str,
                 destination_table_name: str,
                 redshift_conn_id: str,
                 aws_credentials_id: str,
                 create_table_sql: str,
                 ignore_headers: int = 1,
                 load_behavior: LoadBehavior = LoadBehavior.TRUNCATE_AND_LOAD,
                 format_as_json: str = 'auto',
                 region: str = 'us-west-2',
                 *args, **kwargs):
        """

        :param create_table_sql:
        :param load_behavior: Behavior for loading SQL File
        :param format_as_json: Location of JSON format metadata file
        :param region: AWS Region for S3 Bucket
        :param s3_bucket_name: Name of source S3 Bucket
        :param s3_key_name: Name of source S3 Key
        :param destination_table_name: Name of destination table
        :param redshift_conn_id: Name of RedShift connection configured in Airflow UI
        :param aws_credentials_id: Name of AWS credentials configured in Airflow UI
        :param ignore_headers: Tell the copy command to ignore headers or not.
        """
        super(StageToRedshiftOperator, self).__init__(*args, **kwargs)
        self.s3_bucket = s3_bucket_name
        self.s3_key = s3_key_name
        self.table = destination_table_name
        self.redshift = redshift_conn_id
        self.aws_credentials = aws_credentials_id
        self.create_table = create_table_sql
        self.ignore_headers = ignore_headers
        self.load_behavior = load_behavior
        self.format_as_json = format_as_json
        self.region = region

    def execute(self, context):
        aws_hook = AwsHook(self.aws_credentials)
        credentials = aws_hook.get_credentials()
        redshift = PostgresHook(self.redshift)

        table_exist_check = redshift.get_records(f"""
                            select count(*) as if_exists
                            from pg_table_def
                            where schemaname = 'public'
                            and tablename = '{self.table}';
                """)

        if table_exist_check[0][0] == 0:
            self.log.info(f"Table {self.table} does not exist.  Executing the creation SQL.")
            redshift.run(self.create_table)

        if self.load_behavior == LoadBehavior.TRUNCATE_AND_LOAD:
            self.log.info("Clearing data from destination Redshift table")
            redshift.run(f"TRUNCATE TABLE {self.table}")

        self.log.info("Copying data from S3 to RedShift")
        s3_path = f"s3://{self.s3_bucket}/{self.s3_key}/"
        formatted_sql = self.copy_sql.format(
            self.table,
            s3_path,
            credentials.access_key,
            credentials.secret_key,
            self.ignore_headers,
            self.region,
            self.format_as_json
        )
        redshift.run(formatted_sql)
