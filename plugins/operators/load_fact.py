from airflow.hooks.postgres_hook import PostgresHook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults

from helpers.load_behavior import LoadBehavior


class LoadFactOperator(BaseOperator):
    ui_color = '#F98866'

    @apply_defaults
    def __init__(self,
                 redshift_conn_id: str,
                 load_fact_table_sql: str,
                 fact_table_name: str,
                 create_table_sql: str,
                 load_behavior: LoadBehavior = LoadBehavior.APPEND,
                 *args, **kwargs):
        """

        :param redshift_conn_id: The RedShift connection Id name configured in AirFlow UI.
        :param load_fact_table_sql: SQL Statement to load the fact table.
        :param fact_table_name: The name of the fact table
        :param load_behavior: Append to the fact table?  False will truncate and reload the table.
        :param create_table_sql: SQL to create the table.
        """
        super(LoadFactOperator, self).__init__(*args, **kwargs)
        self.redshift_conn_id = redshift_conn_id
        self.sql = load_fact_table_sql
        self.table = fact_table_name
        self.load_behavior = load_behavior
        self.create_table = create_table_sql

    # noinspection DuplicatedCode
    def execute(self, context):
        redshift = PostgresHook(self.redshift_conn_id)

        table_exist_check = redshift.get_records(f"""
                            select count(*) as if_exists
                            from pg_table_def
                            where schemaname = 'public'
                            and tablename = '{self.table}';
                """)

        if table_exist_check[0][0] == 0:
            self.log.info(f"Table {self.table} does not exist.  Executing the creation SQL.")
            redshift.run(self.create_table)

        if self.load_behavior == LoadBehavior.TRUNCATE_AND_LOAD:
            self.log.info(f"Clearing table {self.table} for TRUNCATE AND LOAD behavior.")
            redshift.run(f"TRUNCATE {self.table};")

        self.log.info(f"Table {self.table} load begin.")
        redshift.run(f"INSERT INTO {self.table} ({self.sql});")
        self.log.info(f"Table {self.table} load complete.")
