class SqlTest:
    def __init__(self, sql_test: str, expected_value: int, error_message: str):
        self.sql_test = sql_test
        self.expected_value = expected_value
        self.error_message = error_message
