from typing import List

from airflow.hooks.postgres_hook import PostgresHook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults

from helpers.sql_test import SqlTest


class DataQualityOperator(BaseOperator):
    ui_color = '#89DA59'

    @apply_defaults
    def __init__(self,
                 redshift_conn_id: str,
                 sql_tests: List[SqlTest],
                 *args, **kwargs):
        super(DataQualityOperator, self).__init__(*args, **kwargs)
        self.redshift_conn_id = redshift_conn_id
        self.sql_tests = sql_tests

    def execute(self, context):
        redshift = PostgresHook(self.redshift_conn_id)
        number_of_failures = 0

        self.log.info("Beginning validation tests.")
        for test in self.sql_tests:
            records = redshift.get_records(test.sql_test)
            if test.expected_value != records[0][0]:
                number_of_failures = number_of_failures + 1
                self.log.error(test.error_message)

        if number_of_failures > 0:
            self.log.error(
                f"{number_of_failures} data quality check(s) failed. See previously logged errors for details.")
            raise ValueError(
                f"{number_of_failures} data quality check(s) failed. See previously logged errors for details.")
        else:
            self.log.info("Validation tests completed without errors.")
