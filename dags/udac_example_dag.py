from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator

from helpers.load_behavior import LoadBehavior
from helpers.sql_queries import SqlQueries
from helpers.create_tables import CreateTables
from helpers.data_quality_checks import DataQualityChecks
from operators import (StageToRedshiftOperator, LoadFactOperator,
                       LoadDimensionOperator, DataQualityOperator)

default_args = {
    'owner': 'udacity',
    'start_date': datetime(2019, 1, 12),
    'depends_on_past': False,
    'retries': 3,
    'retry_delay': timedelta(minutes=5),
    'email_on_retry': False,
}

dag = DAG('udac_example_dag',
          default_args=default_args,
          description='Load and transform data in Redshift with Airflow',
          schedule_interval='0 * * * *',
          catchup=False
          )

start_operator = DummyOperator(task_id='Begin_execution', dag=dag)

stage_events_to_redshift = StageToRedshiftOperator(
    task_id='Stage_events',
    dag=dag,
    redshift_conn_id="redshift",
    aws_credentials_id="aws_credentials",
    s3_bucket_name="udacity-dend",
    s3_key_name="log_data",
    destination_table_name="staging_events",
    create_table_sql=CreateTables.staging_events_table,
    load_behavior=LoadBehavior.TRUNCATE_AND_LOAD
)

stage_songs_to_redshift = StageToRedshiftOperator(
    task_id='Stage_songs',
    dag=dag,
    redshift_conn_id="redshift",
    aws_credentials_id="aws_credentials",
    s3_bucket_name="udacity-dend",
    s3_key_name="song_data",
    destination_table_name="staging_songs",
    create_table_sql=CreateTables.staging_songs_table,
    load_behavior=LoadBehavior.TRUNCATE_AND_LOAD
)

load_songplays_table = LoadFactOperator(
    task_id='Load_songplays_fact_table',
    dag=dag,
    redshift_conn_id="redshift",
    fact_table_name="songplays",
    load_fact_table_sql=SqlQueries.songplay_table_insert,
    create_table_sql=CreateTables.songplays_table,
    load_behavior=LoadBehavior.APPEND
)

load_user_dimension_table = LoadDimensionOperator(
    task_id='Load_user_dim_table',
    dag=dag,
    redshift_conn_id="redshift",
    dimension_table_name="users",
    load_dimension_table_sql=SqlQueries.user_table_insert,
    create_table_statement=CreateTables.users_table,
    load_behavior=LoadBehavior.TRUNCATE_AND_LOAD
)

load_song_dimension_table = LoadDimensionOperator(
    task_id='Load_song_dim_table',
    dag=dag,
    redshift_conn_id="redshift",
    dimension_table_name="songs",
    load_dimension_table_sql=SqlQueries.song_table_insert,
    create_table_statement=CreateTables.songs_table,
    load_behavior=LoadBehavior.TRUNCATE_AND_LOAD
)

load_artist_dimension_table = LoadDimensionOperator(
    task_id='Load_artist_dim_table',
    dag=dag,
    redshift_conn_id="redshift",
    dimension_table_name="artists",
    load_dimension_table_sql=SqlQueries.artist_table_insert,
    create_table_statement=CreateTables.artists_table,
    load_behavior=LoadBehavior.TRUNCATE_AND_LOAD
)

load_time_dimension_table = LoadDimensionOperator(
    task_id='Load_time_dim_table',
    dag=dag,
    redshift_conn_id="redshift",
    dimension_table_name="time",
    load_dimension_table_sql=SqlQueries.time_table_insert,
    create_table_statement=CreateTables.time_table,
    load_behavior=LoadBehavior.TRUNCATE_AND_LOAD
)

run_quality_checks = DataQualityOperator(
    task_id='Run_data_quality_checks',
    dag=dag,
    redshift_conn_id="redshift",
    sql_tests=DataQualityChecks.get_all()
)

end_operator = DummyOperator(task_id='Stop_execution', dag=dag)

start_operator >> stage_events_to_redshift
start_operator >> stage_songs_to_redshift

stage_events_to_redshift >> load_songplays_table
stage_songs_to_redshift >> load_songplays_table

load_songplays_table >> load_song_dimension_table
load_songplays_table >> load_user_dimension_table
load_songplays_table >> load_artist_dimension_table
load_songplays_table >> load_time_dimension_table

load_song_dimension_table >> run_quality_checks
load_user_dimension_table >> run_quality_checks
load_artist_dimension_table >> run_quality_checks
load_time_dimension_table >> run_quality_checks

run_quality_checks >> end_operator
