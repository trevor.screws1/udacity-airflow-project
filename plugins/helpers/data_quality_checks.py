from helpers.sql_test import SqlTest


class DataQualityChecks:
    class Test:
        def __init__(self, table: str, column: str, expected_value: int):
            self.table = table
            self.column = column
            self.expected_value = expected_value

    __dimension_tables = [
        Test("artists", "artistid", 0),
        Test("songs", "songid", 0),
        Test("time", "start_time", 0),
        Test("users", "userid", 0)
    ]

    __fact_tables = [
        Test("songplays", "songid", 0)
    ]

    __sql_template = """
    SELECT COUNT(*) 
    FROM {table}
    WHERE {column} IS NULL;
    """

    __error_message = "Error! {table} failed data check on column: {column}"

    @staticmethod
    def get_all():
        dimensions = DataQualityChecks.__dimension_tables
        facts = DataQualityChecks.__fact_tables
        all_tests = dimensions + facts

        sql = DataQualityChecks.__sql_template
        error_message = DataQualityChecks.__error_message
        sql_list = []

        for test in all_tests:
            table, column, expected_value = test.table, test.column, test.expected_value
            formatted_sql = sql.format(table=table, column=column)
            formatted_error_message = error_message.format(table=table, column=column)
            sql_list.append(SqlTest(formatted_sql, expected_value, formatted_error_message))

        return sql_list
