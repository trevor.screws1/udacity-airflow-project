from airflow.hooks.postgres_hook import PostgresHook
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults

from helpers.load_behavior import LoadBehavior


class LoadDimensionOperator(BaseOperator):
    ui_color = '#80BD9E'

    @apply_defaults
    def __init__(self,
                 redshift_conn_id: str,
                 load_dimension_table_sql: str,
                 dimension_table_name: str,
                 load_behavior: LoadBehavior = LoadBehavior.TRUNCATE_AND_LOAD,
                 create_table_statement: str = "",
                 *args, **kwargs):
        """
        Initialize a Load Dimension Operator.

        :param redshift_conn_id: The RedShift connection Id name configured in AirFlow UI.
        :param load_dimension_table_sql: SQL Statement to load the dimension table.
        :param dimension_table_name: The name of the dimension table
        :param load_behavior: Should the table be truncated prior to loading?
        :param create_table_statement: If the table is truncated, provide the SQL statement to re-create the table.
        """
        super(LoadDimensionOperator, self).__init__(*args, **kwargs)
        self.redshift_conn_id = redshift_conn_id
        self.sql = load_dimension_table_sql
        self.table = dimension_table_name
        self.load_behavior = load_behavior
        self.create_table = create_table_statement

    # noinspection DuplicatedCode
    def execute(self, context):
        redshift = PostgresHook(self.redshift_conn_id)

        table_exist_check = redshift.get_records(f"""
                            select count(*) as if_exists
                            from pg_table_def
                            where schemaname = 'public'
                            and tablename = '{self.table}';
                """)

        if table_exist_check[0][0] == 0:
            self.log.info(f"Table {self.table} does not exist.  Executing the creation SQL.")
            redshift.run(self.create_table)

        if self.load_behavior == LoadBehavior.TRUNCATE_AND_LOAD:
            self.log.info(f"Clearing table {self.table} for TRUNCATE AND LOAD behavior.")
            redshift.run(f"TRUNCATE TABLE {self.table};")

        self.log.info(f"Table {self.table} load begin.")
        redshift.run(f"INSERT INTO {self.table} ({self.sql});")
        self.log.info(f"Table {self.table} load complete.")
