# Sparkify ETL Pipeline
Sparkify has grown their user base and song database and wants to move their processes and data onto the cloud. They have two types of data stored in S3: music metadata and user activity logs.  Sparkify's data analysts need a data warehouse to use for analytical queries and BI tools.

This application establishes an ETL pipeline from the raw data stored in the Amazon S3 buckets to a data warehouse hosted on Amazon Redshift.  Scheduling and management is performed via Apache Airflow.

## Getting Started
**Step 1:** Start airstream
Run `/opt/airflow/start.sh` to start airflow.

**Step 2:** Configure connections in Apache Airflow
* Go to the Airflow Web Portal UI
* Click on the Admin > Connections link
* Create a new connection for AWS as:
  * Conn Id: Enter aws_credentials.
  * Conn Type: Enter Amazon Web Services.
  * Login: Enter your Access key ID from the IAM User credentials you downloaded earlier.
  * Password: Enter your Secret access key from the IAM User credentials you downloaded earlier.
* Once you've entered these values, select Save and Add Another.
* Create a new connection for Redshift as:
   * Conn Id: Enter redshift.
   * Conn Type: Enter Postgres.
   * Host: Enter the endpoint of your Redshift cluster, excluding the port at the end. You can find this by selecting your cluster in the Clusters page of the Amazon Redshift console. See where this is located in the screenshot below. IMPORTANT: Make sure to NOT include the port at the end of the Redshift endpoint string.
   * Schema: Enter dev. This is the Redshift database you want to connect to.
   * Login: Enter awsuser.
   * Password: Enter the password you created when launching your Redshift cluster.
   * Port: Enter 5439.

**Step 3:** Turn on the DAG and Run It
This will execute the DAG for the first time.  Leave the DAG on and it will run hourly.

## Source Data
The source data is located in AWS S3 Buckets:
* Song data: `s3://udacity-dend/song_data`
* Log data: `s3://udacity-dend/log_data`

### Song Data
The files are partitioned by the first three letters of each song's track ID. For example, these are filepaths to files in this dataset:

`song_data/A/B/C/TRABCEI128F424C983.json`
`song_data/A/A/B/TRAABJL12903CDCF1A.json`


And below is an example of what a single song file, TRAABJL12903CDCF1A.json, looks like.

```
{
    "num_songs": 1, 
    "artist_id": "ARJIE2Y1187B994AB7", 
    "artist_latitude": null, 
    "artist_longitude": null, 
    "artist_location": "", 
    "artist_name": "Line Renaud", 
    "song_id": "SOUPIRU12A6D4FA1E1", 
    "title": "Der Kleine Dompfaff", 
    "duration": 152.92036, 
    "year": 0
}
```

### Log Data

The log files in the dataset are partitioned by year and month. For example, here are filepaths to two files in this dataset.

`log_data/2018/11/2018-11-12-events.json`
`log_data/2018/11/2018-11-13-events.json`

#### Data Example
![Source Data Example Image](readme_images/log-data.png)

#### List of Fields

* artist,
* auth,
* firstName,
* gender,
* itemInSession,
* lastName,
* length,
* level,
* location,
* method,
* page,
* registration,
* sessionId,
* song,
* status,
* ts,
* userAgent,
* userId

## Redshift Schema for Staging
The schema for the staging area is a 1-to-1 match with the file structures identified above.  One table for song data, `staging_songs` and one table for log data `staging_events`.

The data is loaded into volatile staging tables for performance gains when populating the data warehouse's dimension and fact tables.

### Schema Design Justifications
Since these tables are loaded by the copy command and are a 1-to-1 match with the source files, no distribution key or sort keys are specified.

## Data Warehouse Schema for Analytics
A star schema is implemented for performance and ease of use by data analysts.
![Analytics Schema](readme_images/analytics-schema.png)
